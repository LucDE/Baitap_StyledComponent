import ToDoList from "./BaiTapStyledComponent/ToDoList/ToDoList";

function App() {
  return (
    <div className="App p-5">
      <ToDoList />
    </div>
  );
}

export default App;
