import { ToDoListDarkTheme } from "../../Themes/ToDoListTheme";
import { arrTheme } from "../../Themes/ToDoListTheme/ThemeManager";
import {
  ADD_TASK,
  CHANGE_THEME,
  DELETE_TASK,
  DONE_TASK,
  UPDATE_TASK,
} from "../types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    {
      id: 1,
      taskName: "task 1",
      done: true,
    },
    {
      id: 2,
      taskName: "task 2",
      done: false,
    },
    {
      id: 3,
      taskName: "task 3",
      done: false,
    },
    {
      id: 4,
      taskName: "task 4",
      done: true,
    },
  ],
  taskEdit: {},
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TASK: {
      if (payload.taskName.trim() === "") {
        alert("Task name is required");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.taskName === payload.taskName;
      });
      if (index !== -1) {
        alert("task name already exists!");
      }
      taskListUpdate.push(payload);

      state.taskList = taskListUpdate;
      return { ...state };
    }
    case CHANGE_THEME: {
      //
      let theme = arrTheme.find((theme) => {
        return theme.id === Number(payload);
      });
      if (theme) {
        state.themeToDoList = { ...theme.theme };
      }
      return { ...state };
    }
    case DONE_TASK: {
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === payload;
      });
      if (index !== -1) {
        taskListUpdate[index].done = true;
      }
      return { ...state, taskList: taskListUpdate };
    }
    case DELETE_TASK: {
      return {
        ...state,
        taskList: state.taskList.filter((task) => task.id !== payload),
      };
    }
    case "edit_task": {
      return { ...state, taskEdit: payload };
    }
    case UPDATE_TASK: {
      state.taskEdit.taskName = payload;
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.id === state.taskEdit.id;
      });
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
        state.taskEdit = { ...state.tasEdit, id: "1" };
      }
      return {
        ...state,
        taskList: taskListUpdate,
      };
    }
    default:
      return state;
  }
};
